#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include<termios.h>
#include<string.h>
#include<stdint.h>
#include<stdlib.h>

// Compute the MODBUS RTU CRC
uint16_t ModRTU_CRC(uint8_t buf[], int len)
{
uint16_t crc = 0xFFFF;
for (int pos = 0; pos < len; pos++) {
    crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc
for (int i = 8; i != 0; i--) { // Loop over each bit
if ((crc & 0x0001) != 0) { // If the LSB is set
    crc >>= 1; // Shift right and XOR 0xA001
    crc ^= 0xA001;
}
else // Else LSB is not set
crc >>= 1; // Just shift right
}
}
// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
return crc;
}


int sendResponder(int count, int file, uint8_t msg[], int len ){
   uint16_t crc = ModRTU_CRC(msg, 6);
   msg[6] = (crc >> 4*2);
   msg[7] = (crc);
   if ((count = write(file, msg, len))<0){
         
         perror("Failed to write to the output\n");
         return -1;
      }
   return count;
}


int readResponder(int count, int file, uint8_t receive[]){
   uint16_t crc = ModRTU_CRC(receive, 6);
   receive[6] = (crc >> 4*2);
   receive[7] = (crc);

   if ((count = read(file, (void*)receive, 100))<0){
         perror("Failed to read from the input\n");
         return -1;
      }

      if (count==0) printf("There was no data available to read!\n");
      else {
         uint16_t check_crc =  ModRTU_CRC(receive,6);
         uint16_t received_crc = ((receive[6] << 8) + receive[7]);
         // printf()
         // printf(check_crc);
         // printf(received_crc);

         if (check_crc == received_crc){
            receive[count]=0;  //There is no null character sent by the Arduino

            // printf("The following was read in [%d]: %s\n",count,receive);
            printf("Received reply: ");
            for (int i = 0; i < count; i++){
                  printf("%02x ", receive[i]);
            }
            printf("\n");

      //   printf("The following was read in ")
      }
      }
      return count;

}





int main(int argc, char *argv[]){
   int file, count;

   const size_t MSG_LEN = 8;
   uint8_t msg[MSG_LEN];
   uint8_t receive[MSG_LEN];

   if(argc!=5){
       printf("Invalid number of arguments, exiting!\n");
       return -2;
   }

   if ((file = open("/dev/ttyS0", O_RDWR | O_NOCTTY ))<0){
      perror("UART: Failed to open the file.\n");
      return -1;
   }
   msg[0] = atoi(argv[1]);
   msg[1] = atoi(argv[2]);
   msg[2] = (atoi(argv[3]) >> 4*2);
   msg[3] = (atoi(argv[3]));
   msg[4] = (atoi(argv[4]) >> 4*2);
   msg[5] = (atoi(argv[4]));

   uint16_t crc = ModRTU_CRC(msg, 6);
   msg[6] = (crc >> 4*2);
   msg[7] = (crc);

   printf("Initialized\n");
   count = sendResponder(count, file, msg, MSG_LEN);
      
   count = readResponder(count, file, receive);
   
   struct termios options;

   tcgetattr(file, &options);
   cfmakeraw(&options);
   options.c_cflag = B115200 | CS8 | CREAD | CLOCAL;
   options.c_cc[VMIN] = 8;
   options.c_iflag = IGNPAR;
   tcflush(file, TCIFLUSH);
   tcsetattr(file, TCSANOW, &options);


 
   // uint8_t testCounter = 0;
   while(1){

      // Getting value from arduino 0
      printf("Reading register 11 on Arduino 0\n");
      msg[0] = 0;
      msg[1] = 3;
      msg[3] = 11;
      // msg[3] = testCounter;
      // msg[5] = testCounter;
      

      count = sendResponder(count, file, msg, MSG_LEN);
      
      count = readResponder(count, file, receive);


      printf("Writing register 1 on Arduino 1\n");
      msg[0] = 1;
      msg[1] = 6;
      msg[3] = 1;
      msg[4] = receive[4];
      msg[5] = receive[5];
      // msg[]

      // printf("Arduino 1\n");
      count = sendResponder(count, file, msg, MSG_LEN);
      // uint8_t receive[8];
      count = readResponder(count, file, receive);

      printf("Reading register 12 Arduino 0\n");
      msg[0] = 0;
      msg[1] = 3;
      msg[3] = 12;
      
      count = sendResponder(count, file, msg, MSG_LEN);
      
      count = readResponder(count, file, receive);
      // usleep(1000000);
      // testCounter ++;

      msg[0] = 1;
      msg[1] = 6;
      msg[3] = 2;
      msg[4] = receive[4];
      msg[5] = receive[5];
      // msg[]

      printf("Writing register 2 on Arduino 1\n");
      count = sendResponder(count, file, msg, MSG_LEN);
      // uint8_t receive[8];
      count = readResponder(count, file, receive);

      printf("Reading register 3 on Arduino 1\n");
      msg[0] = 1;
      msg[1] = 3;
      msg[3] = 3;
      
      count = sendResponder(count, file, msg, MSG_LEN);
      
      count = readResponder(count, file, receive);

      printf("Reading register 4 on Arduino 1\n");
      msg[0] = 1;
      msg[1] = 3;
      msg[3] = 4;
      
      count = sendResponder(count, file, msg, MSG_LEN);
      
      count = readResponder(count, file, receive);


      usleep(100000);
      // usleep(100000);
      
   }

   close(file);
   return 0;
}





// int mod_split(uint64_t message)
// {
//     uint8_t slave = (message >> 4*14);
//     uint8_t function = (message >> 4*12) & 0xFF;
//     uint16_t reg = (message >> 4*8) & 0xFFFF;
//     uint16_t data = (message >> 4*4) & 0xFFFF;
//     uint16_t crc = message & 0xFFFF;
//     // printf("Slave: %x\n",slave);
//     // printf("Function: %x\n",function);
//     // printf("Register: %x\n",reg);
//     // printf("Data: %x\n",data);
//     // printf("CRC: %x\n",crc);

//     return 0;
// }

