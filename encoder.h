#include <stdint.h>
#ifndef ENCODER_H
#define ENCODER_H

class Encoder
{

    public:
        Encoder();
        void init();
        int counter;

    private:
        bool old_int0;
        bool old_int1;
        bool current_enc2;
        bool current_enc1;
        bool old_enc1;
        bool old_enc2;
        bool enc1_change;
        bool enc2_change;
};


#endif
