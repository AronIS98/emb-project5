#include <Arduino.h>
#include "encoder.h"
#include "controler.h"


const uint8_t my_number = 1;


uint16_t ModRTU_CRC(uint8_t buf[], int len)
{
uint16_t crc = 0xFFFF;
for (int pos = 0; pos < len; pos++) {
    crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc
for (int i = 8; i != 0; i--) { // Loop over each bit
if ((crc & 0x0001) != 0) { // If the LSB is set
    crc >>= 1; // Shift right and XOR 0xA001
    crc ^= 0xA001;
}
else // Else LSB is not set
crc >>= 1; // Just shift right
}
}
// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
return crc;
}

const uint16_t operational = 1;
const uint16_t stop = 2;
const uint16_t pre_op = 80;
const uint16_t reset_node = 81;
const uint16_t reset_com = 82;
Controler contr;



Encoder enc;
Controler contr1;
Controler contr2;


const uint16_t nr_of_registers = 100;
uint16_t my_register[nr_of_registers];

uint8_t old_case = 0;
uint16_t encoder_counter1 = 0;
uint16_t encoder_counter2 = 0;
volatile int speed_timer = 10;
long old_micro = micros();
long new_micro = micros();

void setup() {            // called once on start up
   // A baud rate of 115200 (8-bit with No parity and 1 stop bit)
   Serial.begin(115200, SERIAL_8N1);
   pinMode(LED_BUILTIN, OUTPUT);         // the LED is an output
//    pinMode(2, OUTPUT);         // the LED is an output
//    pinMode(3, OUTPUT);         // the LED is an output
   pinMode(4, OUTPUT);         // the LED is an output
   pinMode(5, OUTPUT);         // the LED is an output
   pinMode(6, OUTPUT);         // the LED is an output
   pinMode(9, OUTPUT);
   pinMode(10, OUTPUT);
   enc.init();

   my_register[3] = 0;
   my_register[4] = 0;

}
void loop() {                                   // loops forever

   uint8_t command[8];

   uint8_t buffer[8];                            // stores the return buffer on each loop   
   if(Serial.available()>0){                     // bytes received

        Serial.readBytes(command,8);             // reads x many bytes

        uint8_t  client   = ( command[0] );
        uint8_t  function = ( command[1] );
        uint16_t reg      = ((command[2] << 8) + command[3]);
        uint16_t data     = ((command[4] << 8) + command[5]);
        uint16_t crc      = ((command[6] << 8) + command[7]);

        uint16_t check_crc = ModRTU_CRC(command, 6);
    
    if (check_crc == crc){

        if (client == my_number){
            digitalWrite(LED_BUILTIN, HIGH);
            
            if(function == 3)
            {
              buffer[0] = command[0];
              buffer[1] = command[1];
              buffer[2] = command[2];
              buffer[3] = command[3];
              uint8_t output_1 = (my_register[reg] >> 8);
              uint8_t output_2 = (my_register[reg] & 0xFF);
              buffer[4] = output_1;
              buffer[5] = output_2;
              uint16_t crc_out = ModRTU_CRC(buffer, 6);
              uint8_t crc1 = (crc_out >>8);
              uint8_t crc2 = crc_out & 0xFF;
              buffer[6] = crc1;
              buffer[7] = crc2;
              Serial.write(buffer,8);
            }
            else if(function == 6)
            {
                buffer[0] = command[0];
                buffer[1] = command[1];
                buffer[2] = command[2];
                buffer[3] = command[3];
                buffer[4] = command[4];
                buffer[5] = command[5];
                buffer[6] = command[6];
                buffer[7] = command[7];
                my_register[reg] = data;
                Serial.write(buffer,8);
            }
            uint16_t new_case = my_register[0];
                switch (new_case){
                    case operational:

                        digitalWrite(4, LOW);
                        digitalWrite(5, LOW);
                        digitalWrite(6, LOW);
                        new_micro = micros();
                        if((new_micro - old_micro) > 10.0e3){
                            
                                old_micro = new_micro;
                                my_register[3] = (encoder_counter1*100.0)/(speed_timer*1.0);
                                my_register[4] = (encoder_counter2*100.0)/(speed_timer*1.0);
                                encoder_counter1 = 0;
                                encoder_counter2 = 0;   
                        }

                        analogWrite(9,contr1.update(my_register[1]*1.4,my_register[3])); //pulses per sec is 0-1400 while signal from RPI is 0-1024, 1.4 is a scaler between them. (1400/1024 = 1.4 ish)
                        analogWrite(10,contr2.update(my_register[2]*1.4,my_register[4]));
                        


                    break;

                    case stop:

                        digitalWrite(4, LOW);
                        digitalWrite(5, LOW);
                        digitalWrite(6, LOW);
                    break;

                    case pre_op:
                        digitalWrite(4, HIGH);
                        digitalWrite(5, LOW);
                        digitalWrite(6, LOW);
                    break;

                    case reset_node:
                        digitalWrite(4, LOW);
                        digitalWrite(5, HIGH);
                        digitalWrite(6, LOW);
                    break;

                    case reset_com:

                        digitalWrite(4, LOW);
                        digitalWrite(5, LOW);
                        digitalWrite(6, HIGH);
                    break;

                }
                old_case = new_case;
        // }
        }
        else{digitalWrite(LED_BUILTIN, LOW);}


   }
   }
}






ISR(INT0_vect)
{
    encoder_counter1++;
}
ISR(INT1_vect)
{
    encoder_counter2++;
}


